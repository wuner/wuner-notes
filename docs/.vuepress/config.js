const fs = require('fs');
const path = require('path');

const getTitle = (srcPath) => {
  let contentList = fs
    .readFileSync(path.join('docs', srcPath, 'README.md'), 'utf-8')
    .split('\n');
  let title = contentList[0].replace('# ', '');
  if (title.includes('[')) {
    title = title.substring(title.indexOf('[') + 1, title.indexOf(']'));
  }
  return { title, isLink: contentList.length > 1 };
};

const dealWithFiles = (result) => {
  let files = [];
  result.forEach((value) => {
    switch (value.leave) {
      default:
        files.push(value.path);
        break;
      case 2:
        if (typeof files[files.length - 1] === 'string') {
          let titleObj = getTitle(files[files.length - 1]);
          files[files.length - 1] = {
            title: titleObj.title,
            path: titleObj.isLink ? files[files.length - 1] : undefined,
            children: [value.path],
            collapsable: true,
            sidebarDepth: 2,
          };
        } else {
          files[files.length - 1].children.push(value.path);
        }
        break;
      case 3:
        let children = files[files.length - 1].children;
        if (typeof children[children.length - 1] === 'string') {
          let titleObj = getTitle(children[children.length - 1]);
          files[files.length - 1].children[children.length - 1] = {
            title: titleObj.title,
            path: titleObj.isLink ? children[children.length - 1] : undefined,
            children: [value.path],
            collapsable: true,
            sidebarDepth: 2,
          };
        } else {
          files[files.length - 1].children[children.length - 1].children.push(
            value.path,
          );
        }
        break;
      case 4:
        let children2 = files[files.length - 1].children;
        let children3 = children2[children2.length - 1].children;
        if (typeof children3[children3.length - 1] === 'string') {
          let titleObj = getTitle(children3[children3.length - 1]);
          files[files.length - 1].children[children2.length - 1].children[
            children3.length - 1
          ] = {
            title: titleObj.title,
            path: titleObj.isLink ? children3[children3.length - 1] : undefined,
            children: [value.path],
            collapsable: true,
            sidebarDepth: 2,
          };
        } else {
          files[files.length - 1].children[children2.length - 1].children[
            children3.length - 1
          ].children.push(value.path);
        }
        break;
    }
  });

  return files;
};

const getQuestionsChildren = (srcPath) => {
  let files = [];

  if (fs.existsSync(path.join('docs', srcPath, 'code'))) {
    const result = readDirSync(path.join('docs', srcPath, 'code'));
    files = dealWithFiles(result);
    files.unshift(srcPath);
  }

  return files;
};

const getChildren = (srcPath) => dealWithFiles(readDirSync(srcPath));

const readDirSync = (dirPath, leave = 0) => {
  let result = [];
  const files = fs.readdirSync(dirPath);
  files.forEach(function (file) {
    let leaveTe = leave;
    const info = fs.statSync(path.join(dirPath, file));
    if (info.isDirectory()) {
      result = result.concat(readDirSync(path.join(dirPath, file), ++leaveTe));
    } else {
      if (file.includes('.md') && !dirPath.includes('//'))
        result.push({
          path:
            dirPath
              .replace(/\\/g, '/')
              .replace(/\bdocs\//g, '/')
              .replace(/\/$/, '') + '/',
          leave,
        });
    }
  });
  return result;
};

const keywords = [
  'Study Notes',
  'Wuner',
  'Vue',
  '前端学习笔记',
  '函数式编程',
  'Promise',
  'ES6+',
  'TypeScript',
  'JS性能优化',
  'cli',
  '脚手架',
  '自动化构建',
  'yeoman',
  'plop',
  'gulp',
  'grunt',
  'ESModule',
  'webpack',
  'rollup',
  'lint',
  'vue-router',
  'vue-observe',
  'virtual-dom',
  '虚拟dom',
  'vue源码解析',
  'vue响应式原理',
  'vue虚拟dom',
  'vue模板编译',
  'vue组件化',
  'vue状态管理(vuex)',
  '服务端渲染(ssr)',
  'nuxtJs',
  '搭建SSR',
];

const items = [
  {
    text: '函数式编程与 JS 异步编程、手写 Promise',
    link: '/fed-e-task-01-01/notes/',
  },
  {
    text: 'ES 新特性与 TypeScript、JS 性能优化',
    link: '/fed-e-task-01-02/notes/',
  },
  {
    text: '开发脚手架及封装自动化构建工作流',
    link: '/fed-e-task-02-01/notes/',
  },
  {
    text: '模块化开发与规范化标准',
    link: '/fed-e-task-02-02/notes/',
  },
  {
    text: '手写 Vue Router、手写响应式实现、虚拟 DOM 和 Diff 算法',
    link: '/fed-e-task-03-01/notes/',
  },
  {
    text: 'Vue.js 源码分析（响应式、虚拟 DOM、模板编译和组件化）',
    link: '/fed-e-task-03-02/notes/',
  },
  {
    text: 'Vuex 数据流管理及Vue.js 服务端渲染（SSR）',
    link: '/fed-e-task-03-03/notes/',
  },
  {
    text: '搭建自己的SSR、静态站点生成（SSG）及封装 Vue.js 组件库',
    link: '/fed-e-task-03-04/notes/',
  },
  {
    text: 'Vue.js 3.0 Composition APIs 及 3.0 原理剖析',
    link: '/fed-e-task-03-05/notes/',
  },
  {
    text: 'Vue.js + Vuex + TypeScript 实战项目开发与项目优化',
    link: '/fed-e-task-03-06/notes/',
  },
];

const createSidebar = () => {
  let obj = {};
  for (let item of items) {
    let qLink = item.link.replace('notes/', '');
    obj[item.link] = getChildren(`docs${item.link}`);
    obj[qLink] = getQuestionsChildren(qLink);
  }

  fs.writeFileSync(
    path.resolve(__dirname, 'children.js'),
    JSON.stringify({ items, children: obj }),
  );
  return obj;
};

const getSidebar = () => {
  let obj = {};
  try {
    const content = fs.readFileSync(path.resolve(__dirname, 'children.js'));
    if (content.items === items) {
      obj = content.children;
    } else {
      obj = createSidebar();
    }
    console.log(1);
  } catch (e) {
    obj = createSidebar();
    console.log(2);
  }

  return obj;
};

module.exports = {
  title: 'Study Notes',
  description: '大前端学习笔记',
  head: [
    [
      'meta',
      {
        name: 'keywords',
        content: keywords.join(','),
      },
    ],
  ],
  base: '/wuner-notes/',
  themeConfig: {
    algolia: {
      apiKey: 'aac02626df262080e66689b38d46091b',
      indexName: 'wuner-notes',
      algoliaOptions: {
        hitsPerPage: 10,
      },
    },
    sidebarDepth: 2,
    nav: [
      {
        text: '博客',
        link: 'https://blog.csdn.net/qq_32090185',
      },
      {
        text: '笔记',
        items,
      },
      {
        text: '题目',
        items: items.map(({ text, link }) => {
          link = link.replace('notes/', '');
          return { text, link };
        }),
      },
      {
        text: '其他',
        items: [
          {
            text: '面试题',
            link: '/interview-questions/w-001-js-base/',
          },
          {
            text: 'socket聊天',
            link: '/chat/',
          },
        ],
      },
    ],
    sidebar: Object.assign({}, getSidebar(), {
      '/interview-questions/': getChildren('docs/interview-questions/'),
      '/chat/': getChildren('docs/chat/'),
    }),
  },
  plugins: [
    [
      'copyright',
      {
        authorName: {
          'en-US': 'Wuner',
          'zh-CN': 'Wuner',
        },
        minLength: 20,
        clipboardComponent: path.resolve(
          __dirname,
          'components/ClipboardComponent.vue',
        ),
      },
    ],
    [
      'vuepress-plugin-medium-zoom',
      {
        selector: 'img',
        options: {
          background: 'rgba(0,0,0,0.7)',
          scrollOffset: 0,
        },
      },
    ],
    [
      'vuepress-plugin-code-copy',
      {
        align: 'top',
        successText: '已复制',
        color: '#fff',
      },
    ],
  ],
  markdown: { lineNumbers: true, extractHeaders: ['h2', 'h3', 'h4'] },
};
