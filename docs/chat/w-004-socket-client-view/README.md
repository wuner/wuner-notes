# 搭建 socket 客户端页面开发

## 目录结构

```text
├── assets
│   └── avatar.png # 自行下载头像并存储到该位置
├── store
│   └── modules
│   │   └── user.js
│   └── index.js
├── utils
│   └── socket.js
├── view
│   ├── chat
│   │   ├── components
│   │   │   ├── receive
│   │   │   ├── └── index.vue
│   │   │   ├── send
│   │   │   └── └── index.vue
│   │   └── index.vue
│   ├── message
│   │   ├── components
│   │   │   ├── cell
│   │   │   └── └── index.vue
│   │   └── index.vue
│   ├── friends
│   │   └── index.vue
│   ├── mine
│   │   └── index.vue
```

## vuex 状态管理

- `store/modules/user.js`
- 用户模块

```js
/**
 * @author Wuner
 * @date 2020/12/9 18:00
 * @description 用户模块
 */
import Session from '@/utils/session';
import { login } from '@/api/user';

const storeState = Session.get('storeState');

const state = (storeState && storeState.user) || {
  // 用户信息
  userinfo: {
    username: '',
    nickname: '',
  },
};

const getters = {};

const mutations = {
  /**
   * 设置用户信息
   * @param state
   * @param data
   */
  setUserinfo(state, data) {
    state.userinfo = data;
  },
};

const actions = {};

export default { namespaced: true, state, getters, mutations, actions };
```

- 初始化 vuex
- `store/index.js`

```js
/**
 * @author Wuner
 * @date 2020/12/9 17:58
 * @description store 配置文件
 */
import Vuex from 'vuex';
import Vue from 'vue';
import Session from '@/utils/session';
import user from './modules/user';

Vue.use(Vuex);

const myPlugin = (store) => {
  store.subscribe((mutation, state) => {
    // 缓存状态
    Session.set('storeState', state);
  });
};

const storeState = Session.get('storeState');
const state = {
  data: (storeState && storeState.data) || {},
};

const getters = {
  /**
   * data数据
   * @param state
   * @returns {*}
   */
  data(state) {
    return state.data;
  },
};

const mutations = {
  /**
   * 设置data数据
   * @param state
   * @param data
   */
  setData(state, data) {
    state.data = data;
  },
};

const actions = {};

const store = new Vuex.Store({
  // 非生产环境添加严格模式
  strict: process.env.NODE_ENV !== 'production',
  state,
  getters,
  mutations,
  actions,
  // 模块
  modules: {
    user,
  },
  // 插件
  plugins: [myPlugin],
});

export default store;
```

## socket 开发

- `utils/socket.js`

```js
/**
 * @returns {String}
 */
import io from 'socket.io-client';

function socketUrl() {
  if (process.env.NODE_ENV === 'local') {
    // 测试
    return `http://localhost:3000/`;
  } else {
    // 生产
    return 'http://localhost:3000/';
  }
}

export const createSocket = (username) => {
  // console.log("建立websocket连接")
  // 建立websocket连接，
  const url = socketUrl();
  const socket = (window.socket = io(url, {
    transports: ['websocket'],
  }));
  console.log(window.socket);

  // 监听客户端连接,回调函数会传递本次连接的socket
  socket.on('connect', () => {
    console.log('#connect,', socket.id);
    const id = socket.id.replace(url, '');
    console.log('#connect,', id);

    let obj = {};
    obj[username] = id;
    // 创建聊天室
    socket.emit('createRoom', obj);
  });
};
```

- `main.js`
- 当去首页时，获取 username，并设置到 vuex 里
- 刷新页面的时候，会丢失 socket 对象，所以在刷新页面时，我们需重新创建连接

```js
router.beforeEach((to, from, next) => {
  if (to.name === 'index') {
    const { username } = to.query;
    store.commit('user/setUserinfo', { username, nickname: username });
  }
  if (store.state.user.userinfo.username && !window.socket) {
    createSocket(store.state.user.userinfo.username);
    next();
  } else {
    next();
  }
});
```

## 首页开发

- `view/index.vue`
- 我们使用嵌套路由开发首页

```vue
<template>
  <div class="index">
    <router-view />
    <van-tabbar v-model="active">
      <van-tabbar-item to="/message" icon="chat-o">消息</van-tabbar-item>
      <van-tabbar-item to="/friends" icon="friends-o">联系人</van-tabbar-item>
      <van-tabbar-item to="/mine" icon="user-o">我的</van-tabbar-item>
    </van-tabbar>
  </div>
</template>

<script>
export default {
  name: 'index',
  data() {
    return { active: 0 };
  },
};
</script>

<style scoped lang="less">
.home {
}
</style>
```

## 子路由

### 聊天页面

- 接收组件
- `view/chat/components/receive/index.vue`

```vue
<template>
  <div class="receive">
    <img src="../../../../assets/avatar.png" alt="" />
    <div class="receive-content">
      <p class="name">{{ info.from }}</p>
      <span class="message">{{ info.message }}</span>
    </div>
  </div>
</template>

<script>
export default {
  name: 'receive',
  props: {
    info: Object,
  },
};
</script>

<style scoped lang="less">
.receive {
  position: relative;
  z-index: 1000;
  margin: 0 12px 24px 12px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  line-height: 16px;

  &-content {
    text-align: left;
  }

  .name {
    margin-bottom: 10px;
  }

  .message {
    padding: 8px 16px;
    background: linear-gradient(270deg, #dc99ff 0%, #f9c2ff 100%);
    border-radius: 10px 0 10px 10px;
    color: #333;
    font-size: 14px;
  }

  img {
    width: 40px;
    margin-top: -4px;
  }
}
</style>
```

- 发送组件
- `view/chat/components/send/index.vue`

```vue
<template>
  <div class="send">
    <div class="send-content">
      <p class="name">{{ info.from }}</p>
      <span class="message">{{ info.message }}</span>
    </div>
    <img src="@/assets/avatar.png" alt="" />
  </div>
</template>

<script>
export default {
  name: 'send',
  props: {
    info: Object,
  },
};
</script>

<style scoped lang="less">
.send {
  position: relative;
  z-index: 1000;
  margin: 0 12px 24px 12px;
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
  line-height: 16px;

  &-content {
    text-align: right;
  }

  .name {
    margin-bottom: 10px;
  }

  .message {
    padding: 8px 16px;
    background: linear-gradient(270deg, #dc99ff 0%, #f9c2ff 100%);
    border-radius: 10px 0 10px 10px;
    color: #333;
    font-size: 14px;
  }

  img {
    width: 40px;
    margin-top: -4px;
  }
}
</style>
```

- 聊天页面
- `view/chat/index.vue`

```vue
<template>
  <div class="chat">
    <div class="chat-content"></div>
    <van-tabbar ref="tabbar" placeholder :border="false">
      <div class="tabbar-content">
        <van-field v-model="message"></van-field>
        <van-button @click="onSend">发送</van-button>
      </div>
    </van-tabbar>
  </div>
</template>

<script>
import Vue from 'vue';
import { mapState } from 'vuex';
import Receive from '@/view/chat/components/receive';
import Send from '@/view/chat/components/send';

export default {
  name: 'jsapi',
  components: { Receive, Send },
  data() {
    return {
      message: '',
    };
  },
  computed: {
    ...mapState('user', ['userinfo']),
  },
  methods: {
    /**
     * 渲染问题数据
     * @param data
     */
    receive(data) {
      this.render(Receive, data);
    },
    /**
     * 渲染问题数据
     * @param data
     */
    send(data) {
      this.render(Send, data);
    },
    /**
     * 渲染
     * @param dom
     * @param data
     * @param callback
     */
    render(dom, data = {}, callback = () => {}) {
      // 文本组件生成
      const El = new (Vue.extend(dom))({
        el: document.createElement('div'),
        propsData: { info: data },
      });
      El.$on('click', callback);
      // 添加到content Node节点
      const ChatContentEl = document.querySelector('.chat-content');
      ChatContentEl.appendChild(El.$el);
      this.$nextTick(() => {
        // 滚动到content视图最下面
        this.$refs.content.scrollTop = this.$refs.content.scrollHeight;
      });
    },
    onSend() {
      const { message } = this;
      this.message = '';
      const data = {
        message,
        to: this.$route.params.username,
        from: this.userinfo.username,
      };
      this.send(data);
      window.socket.emit('send', data);
    },
  },
  mounted() {
    window.socket.on('receive', (data) => {
      console.log(data);
      this.receive(data);
    });
  },
};
</script>
<style lang="less">
.chat {
  min-height: 100vh;
  background-color: #f2f2f2;

  &-content {
    padding: 12px 0;
  }
  .van-tabbar {
    background: transparent;
  }
  .tabbar-content {
    width: 90%;
    margin: 0 5%;
    display: flex;
    align-items: center;

    .van-button {
      width: 80px;
      height: 40px;
      border-radius: 5px;
      background-color: green;
      color: white;
      margin-left: 8px;
    }
  }
}
</style>
```

### 消息页面

- 消息单元组件
- `view/message/components/cell/index.vue`

```vue
<template>
  <div class="cell" @click="onClick">
    <div class="cell-left">
      <img :src="info.avatar" alt="" />
      <p>{{ info.nickname }}</p>
    </div>
  </div>
</template>

<script>
export default {
  name: 'cell',
  props: {
    info: Object,
  },
  methods: {
    onClick() {
      this.$emit('click', this.info);
    },
  },
};
</script>

<style scoped lang="less">
.cell {
  margin-bottom: 8px;

  &-left {
    img {
      width: 40px;
      height: 40px;
    }

    p {
      font-size: 16px;
    }
  }
}
</style>
```

- 消息页面
- `view/message/index.vue`

```vue
<template>
  <div class="message">
    <cell
      v-for="item in list"
      :info="item"
      :key="item.username"
      @click="onClick"
    />
  </div>
</template>

<script>
import Cell from '@/view/message/components/cell';
export default {
  name: 'message',
  components: { Cell },
  data() {
    return {
      list: [
        {
          username: 'wuner',
          nickname: 'wuner',
          avatar: require('@/assets/avatar.png'),
        },
        {
          username: 'wuner1',
          nickname: 'wuner1',
          avatar: require('@/assets/avatar.png'),
        },
      ],
    };
  },
  methods: {
    onClick(info) {
      console.log(info);
      this.$router.push({ name: 'chat', params: { username: info.username } });
    },
  },
};
</script>

<style scoped lang="less">
.message {
}
</style>
```

### 联系人页面

`view/friends/index.vue`

```vue
<template>
  <div class="friends">联系人</div>
</template>

<script>
export default {
  name: 'friends',
  data() {
    return {};
  },
};
</script>

<style scoped lang="less">
.friends {
}
</style>
```

### 我的页面

`view/mine/index.vue`

```vue
<template>
  <div class="mine">我的</div>
</template>

<script>
export default {
  name: 'mine',
  data() {
    return {};
  },
};
</script>

<style scoped lang="less">
.mine {
}
</style>
```

## 路由配置

- `router/index.js`

```js
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router); // 启用router插件

// 以下是路由配置
let router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import(/*webpackChunkName: "index"*/ '@/view/index.vue'),
      redirect: '/message',
      children: [
        {
          path: '/message',
          name: 'message',
          component: () =>
            import(/*webpackChunkName: "index"*/ '@/view/message/index.vue'),
          meta: {
            title: '消息',
          },
        },
        {
          path: '/friends',
          name: 'friends',
          component: () =>
            import(/*webpackChunkName: "index"*/ '@/view/friends/index.vue'),
          meta: {
            title: '联系人',
          },
        },
        {
          path: '/mine',
          name: 'mine',
          component: () =>
            import(/*webpackChunkName: "index"*/ '@/view/mine/index.vue'),
          meta: {
            title: '我的',
          },
        },
      ],
    },
    {
      path: '/chat/:username',
      name: 'chat',
      component: () =>
        import(/*webpackChunkName: "index"*/ '@/view/chat/index.vue'),
      meta: {
        title: '聊天',
      },
    },
  ],
});

export default router;
```

## 测试

```cmd
yarn start
# 或者
npm run start
```

这时浏览器会自带打开页面，在浏览器连接中，添加?username=wuner，并复制浏览器连接，将 wuner 改为 wuner1，并使用新建标签打开

![note](./imgs/1.png)

- wuner 进入 wuner1
- wuner1 进入 wuner

![note](./imgs/2.png)

## [项目地址](https://gitee.com/Wuner/socket-chat/tree/chat-page/)
