# 创建项⽬

## 使⽤ Vue CLI 创建项⽬

### 安装 Vue CLI

#### npm

```shell
 npm i -g @vue/cli
```

#### yarn

```shell
yarn global add @vue/cli
```

### 创建项目

```shell
vue create edu-boss-wuner
```

![notes](imgs/1.png)

![notes](imgs/2.png)

### 启动开发服务

```shell
# 进⼊你的项⽬⽬录
cd edu-boss-fed

# 启动开发服务
npm run serve
```

## 初始⽬录结构说明

```markdown
├── node_modules # 第三⽅包存储⽬录
├── public # 静态资源⽬录，任何放置在 public ⽂件夹的静态资源都会被简单的复 制，⽽不经过 webpack
│ ├── favicon.ico
│ └── index.html
├── src
│ ├── assets # 公共资源⽬录，放图⽚等资源
│ ├── components # 公共组件⽬录
│ ├── router # 路由相关模块
│ ├── store # 容器相关模块
│ ├── views # 路由⻚⾯组件存储⽬录
│ ├── App.vue # 根组件，最终被替换渲染到 index.html ⻚⾯中 #app ⼊⼝节点
│ ├── main.ts # 整个项⽬的启动⼊⼝模块
│ ├── shims-tsx.d.ts # ⽀持以 .tsc 结尾的⽂件，在 Vue 项⽬中编写 jsx 代码
│ └── shims-vue.d.ts # 让 TypeScript 识别 .vue 模块
├── .browserslistrc # 指定了项⽬的⽬标浏览器的范围。这个值会被 @babel/preset-env 和 Autoprefixer ⽤来确定需要转译的 JavaScript 特性和需要添加的 CSS 浏览器前缀
├── .eslintrc.js # ESLint 的配置⽂件
├── .gitignore # Git 的忽略配置⽂件，告诉 Git 项⽬中要忽略的⽂件或⽂件夹
├── README.md # 说明⽂档
├── babel.config.js # Babel 配置⽂件
├── package-lock.json # 记录安装时的包的版本号，以保证⾃⼰或其他⼈在 npm install 时⼤家的依赖能保证⼀致
├── package.json # 包说明⽂件，记录了项⽬中使⽤到的第三⽅包依赖信息等内容
└── tsconfig.json # TypeScript 配置⽂件
```

## 调整初始⽬录结构

默认⽣成的⽬录结构不满⾜我们的开发需求，所以需要做⼀些⾃定义改动。

这⾥主要处理下⾯的内容：

- 删除初始化的默认⽂件
- 新增调整我们需要的⽬录结构

### 修改 App.vue

```vue
<template>
  <div id="app">
    <!-- 根级路由出⼝ -->
    <router-view />
  </div>
</template>
```

### 修改 router/index.ts

```ts
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [];

const router = new VueRouter({
  routes,
});

export default router;
```

### 删除默认示例⽂件

- src/views/About.vue
- src/views/Home.vue
- src/components/HelloWorld.vue
- src/assets/logo.png

### 创建以下内容

- src/services ⽬录，接⼝模块
- src/utils ⽬录，存储⼀些⼯具模块
- src/styles ⽬录，存储⼀些样式资源

### 调整之后的⽬录结构如下

```markdown
.
├── public
│ ├── favicon.ico
│ └── index.html
├── src
│ ├── assets
│ ├── components
│ ├── router
│ ├── services
│ ├── store
│ ├── styles
│ ├── utils
│ ├── views
│ ├── App.vue
│ ├── main.ts
│ ├── shims-tsx.d.ts
│ └── shims-vue.d.ts
├── .browserslistrc
├── .eslintrc.js
├── .gitignore
├── README.md
├── babel.config.js
├── package-lock.json
├── package.json
└── tsconfig.json
```

## 使用 TS 开发 Vue 项目

### 在 Vue 项目中启用 TS 支持

#### 全新项目

使⽤ Vue CLI 创建 Vue 项⽬

![notes](imgs/1.png)

#### 已有项目

添加 Vue 官方配置的 TS 适配插件

使用@vue/cli 安装 TS 插件

```shell
vue add @vue/typescript
```

## TS 相关配置介绍

### 安装了 TS 相关的依赖性

#### dependencies 依赖

| 依赖性                 | 说明                                      |
| ---------------------- | ----------------------------------------- |
| vue-class-component    | 提供使用 class 语法写 Vue 组件            |
| vue-property-decorator | 在 class 语法基础之上提供了一些辅助装饰器 |

#### devDependencies 依赖

| 依赖性                           | 说明                                                                 |
| -------------------------------- | -------------------------------------------------------------------- |
| @typescript-eslint/eslint-plugin | 使用 eslint 校验 TS 代码                                             |
| @typescript-eslint/parser        | 将 TS 转换为 AST 提供 eslint 校验使用                                |
| @vue/cli-plugin-typescript       | 使用 TS+ ts-loader+fork-ts-checker-webpack-plugin 进行更快的类型检查 |
| @vue/eslint-config-typescript    | 兼容 eslint 的 TS 校验规则                                           |
| typescript                       | TS 编辑器 ，提供类型校验 和转换 js 功能                              |

### TS 配置文件

tsconfig.json

```json
{
  "compilerOptions": {
    "target": "esnext",
    "module": "esnext",
    "strict": true,
    "jsx": "preserve",
    "importHelpers": true,
    "moduleResolution": "node",
    "experimentalDecorators": true,
    "skipLibCheck": true,
    "esModuleInterop": true,
    "allowSyntheticDefaultImports": true,
    "sourceMap": true,
    "baseUrl": ".",
    "types": ["webpack-env"],
    "paths": {
      "@/*": ["src/*"]
    },
    "lib": ["esnext", "dom", "dom.iterable", "scripthost"]
  },
  "include": [
    "src/**/*.ts",
    "src/**/*.tsx",
    "src/**/*.vue",
    "tests/**/*.ts",
    "tests/**/*.tsx"
  ],
  "exclude": ["node_modules"]
}
```

### shims-vue.d.ts 文件的作用

- 主要用于 TS 识别 .vue 文件模块
- TS 默认不支持导入 .vue 模块，这个文件告诉 TS 导入 .vue 文件模块都按 `VueConstructor<Vue>` 类型识别处理

```ts
declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}
```

### shims-tsx.d.ts 文件的作用

为 jsx 组件模块补充类型声明

```ts
import Vue, { VNode } from 'vue';

declare global {
  namespace JSX {
    // tslint:disable no-empty-interface
    interface Element extends VNode {}
    // tslint:disable no-empty-interface
    interface ElementClass extends Vue {}
    interface IntrinsicElements {
      [elem: string]: any;
    }
  }
}
```

## [TypeScript 支持](https://cn.vuejs.org/v2/guide/typescript.html)
