# 用户模块

## 登陆

### 缓存方法

`src/utils/storage.ts`

```ts
import { RootState } from '@/types';

export const get = function (key: string, storage: Storage = sessionStorage) {
  const value = storage.getItem(key);
  let data;

  if (typeof value === 'string') {
    try {
      data = JSON.parse(value);
    } catch (e) {
      data = value;
    }
  } else {
    data = value;
  }

  return data;
};

export const set = function (
  key: string,
  value: RootState | string | object,
  storage: Storage = sessionStorage,
) {
  if (typeof value === 'object') {
    storage.setItem(key, JSON.stringify(value));
  } else {
    storage.setItem(key, value);
  }
};

export const remove = function (
  key: string,
  storage: Storage = sessionStorage,
) {
  storage.removeItem(key);
};
```

### 接口

#### 封装请求模块

`src/utils/request.ts`

```ts
import axios from 'axios';
import qs from 'qs';
import store from '@/store';
import { Store } from 'vuex';
import { RootState } from '@/types';

const request = axios.create({
  timeout: 10000,
});

// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    const { access_token: accessToken } =
      (store as Store<RootState>).getters['user/userLoginInfo'] || {};
    if (accessToken) {
      config.headers.Authorization = accessToken;
    }
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  },
);

// 添加响应拦截器
request.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    const { data } = response;
    const { state, content, message } = data;
    if (state === 200 || state === 1) {
      return JSON.parse(content);
    } else {
      return Promise.reject(message);
    }
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  },
);

const post = (url: string, data: object) => {
  return request({
    url,
    method: 'POST',
    data: qs.stringify(data),
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  });
};

export { post };
export default request;
```

#### 数据类型

`src/types/index.d.ts`

```ts
export interface User {
  phone: string;
  password: string;
}
```

#### 接口封装

`src/services/user/index.ts`

```ts
import { post } from '@/utils/request';
import { User } from '@/types';

const login = (params: User) => {
  return post('/front/user/login', params);
};

export { login };
```

### vuex 配置

#### 数据类型

`src/types/index.d.ts`

```ts
export interface RootState {
  user: UserState;
}

export interface UserState {
  userInfo: UserInfo;
  userLoginInfo: object;
}

export interface User {
  phone: string;
  password: string;
}

export interface UserInfo {
  isUpdatedPassword: string;
  portrait: string;
  userName: string;
}
```

#### `src/store/index.ts`

```ts
import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import user from './user';
import { set } from '@/utils/storage';
import { RootState } from '@/types';

Vue.use(Vuex);

const myPlugin = (store: Store<RootState>) => {
  // 当 store 初始化后调用
  store.subscribe((mutation, state) => {
    // 每次 mutation 之后调用
    // 将state数据缓存到浏览器会话缓存中
    set('RootState', state);
    // 将用户登陆信息放置到本地缓存，当用户再次打开浏览器时，可以直接使用上次到登陆信息
    if (mutation.type === 'user/setUserLoginInfo') {
      set('userLoginInfo', state.user.userLoginInfo, localStorage);
    }
  });
};

export default new Vuex.Store({
  // @ts-ignore：state中的数据由modules提供
  state: {},
  mutations: {},
  actions: {},
  modules: { user },
  plugins: [myPlugin],
});
```

#### 用户模块配置

```ts
import { UserState } from '@/types';
import { get } from '@/utils/session';

const RootState = get('RootState');
const userLoginInfo = get('userLoginInfo', localStorage);

// 如果缓存中存在用户模块数据，则将缓存中的数据设置给state
const state: UserState = (RootState && RootState.user) ||
  (userLoginInfo && { userLoginInfo }) || {
    userLoginInfo: null, // 用户登陆信息
  };

const getters = {
  /**
   * 获取用户信息
   * @param state
   */
  userLoginInfo(state: UserState) {
    return state.userLoginInfo;
  },
};

const mutations = {
  /**
   * 设置用户信息
   * @param state
   * @param data
   */
  setUserLoginInfo(state: UserState, data: object) {
    state.userLoginInfo = data;
  },
};

const actions = {};

export default { namespaced: true, state, getters, mutations, actions };
```

### 页面

`src/views/login/index.vue`

```vue
<template>
  <div class="login">
    <div class="login-box">
      <h1 class="login-title">Edu boss管理系统</h1>
      <el-card class="login-card">
        <h2>登录</h2>
        <el-form
          label-position="top"
          :model="ruleForm"
          status-icon
          :rules="rules"
          ref="ruleForm"
          class="demo-ruleForm"
        >
          <el-form-item label="手机号" prop="phone">
            <el-input
              type="text"
              clearable
              maxlength="11"
              v-model="ruleForm.phone"
              autocomplete="off"
            ></el-input>
          </el-form-item>
          <el-form-item label="密码" prop="password">
            <el-input
              type="password"
              clearable
              v-model="ruleForm.password"
              autocomplete="off"
            ></el-input>
          </el-form-item>
          <el-form-item>
            <el-button :loading="loginLoading" type="primary" @click="onLogin">
              登录
            </el-button>
          </el-form-item>
        </el-form>
      </el-card>
    </div>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { Form } from 'element-ui';
import { mapMutations } from 'vuex';
import { User } from '@/types';
import { login } from '@/user';

export default Vue.extend({
  name: 'LoginIndex',
  data() {
    const checkPhone = (rule: object, value: string, callback: Function) => {
      if (!value) {
        return callback(new Error('手机号不能为空'));
      }
      if (!value.match(/^[0-9]{11}$/)) {
        callback(new Error('请输入正确手机号'));
      } else {
        callback();
      }
    };
    const validatePassword = (
      rule: object,
      value: string,
      callback: Function,
    ) => {
      if (value === '' || value.length < 6) {
        callback(new Error('密码不能少于6位'));
      } else {
        callback();
      }
    };
    return {
      loginLoading: false,
      ruleForm: {
        password: '111111',
        phone: '15510792995',
      },
      rules: {
        password: [
          {
            required: true,
            validator: validatePassword,
            trigger: ['change', 'blur'],
          },
        ],
        phone: [
          {
            required: true,
            validator: checkPhone,
            trigger: ['change', 'blur'],
          },
        ],
      },
    };
  },
  methods: {
    ...mapMutations('user', ['setUserLoginInfo']),
    onLogin(): void {
      (this.$refs.ruleForm as Form).validate((valid: boolean) => {
        if (valid) {
          this.loginLoading = true;
          this.login(this.ruleForm);
        }
      });
    },
    async login(params: User) {
      try {
        const data = await login(params);
        this.setUserLoginInfo(data);
        this.$router.push('/');
        this.$message({
          message: '登陆成功',
          type: 'success',
        });
      } catch (e) {
        this.$message({
          message: e,
          type: 'error',
        });
      }
      this.loginLoading = false;
    },
  },
});
</script>

<style lang="scss" scoped>
.login {
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;

  &-title {
    text-align: center;
  }

  &-card {
    width: 380px;
  }

  .el-button {
    width: 100%;
  }
}
</style>
```

## 身份认证

### 校验页面访问权限

使用 [前置路由守卫](https://router.vuejs.org/zh/guide/advanced/navigation-guards.html#%E5%85%A8%E5%B1%80%E5%89%8D%E7%BD%AE%E5%AE%88%E5%8D%AB) ，当用户未登录时，不让用户进入需要用户认证的页面

`src/router/index.ts`

```ts
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Layout from '@/layout/index.vue';
import store from '@/store';

Vue.use(VueRouter);

// 路由配置规则
const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'login',
    component: () =>
      import(/* webpackChunkName: 'login' */ '@/views/login/index.vue'),
  },
  {
    path: '/',
    component: Layout,
    meta: { isAuthenticated: true },
    children: [
      {
        path: '', // 默认子路由
        name: 'home',
        component: () =>
          import(/* webpackChunkName: 'home' */ '@/views/home/index.vue'),
      },
      {
        path: '/role',
        name: 'role',
        component: () =>
          import(/* webpackChunkName: 'role' */ '@/views/role/index.vue'),
      },
      {
        path: '/menu',
        name: 'menu',
        component: () =>
          import(/* webpackChunkName: 'menu' */ '@/views/menu/index.vue'),
      },
      {
        path: '/resource',
        name: 'resource',
        component: () =>
          import(
            /* webpackChunkName: 'resource' */ '@/views/resource/index.vue'
          ),
      },
      {
        path: '/course',
        name: 'course',
        component: () =>
          import(/* webpackChunkName: 'course' */ '@/views/course/index.vue'),
      },
      {
        path: '/user',
        name: 'user',
        component: () =>
          import(/* webpackChunkName: 'user' */ '@/views/user/index.vue'),
      },
      {
        path: '/advert',
        name: 'advert',
        component: () =>
          import(/* webpackChunkName: 'advert' */ '@/views/advert/index.vue'),
      },
      {
        path: '/advert-space',
        name: 'advert-space',
        component: () =>
          import(
            /* webpackChunkName: 'advert-space' */ '@/views/advert-space/index.vue'
          ),
      },
    ],
  },

  {
    path: '*',
    name: '404',
    component: () =>
      import(/* webpackChunkName: '404' */ '@/views/error-page/404.vue'),
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  if (!to.matched.some((value) => value.meta.isAuthenticated)) {
    next();
  } else {
    if (store.getters['user/userLoginInfo']) {
      next();
    } else {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath, // 用于判断用户token失效时，跳转到登陆页，当用户登陆后，可重新跳转回去之前当页面
        },
      });
    }
  }
});

export default router;
```

### 展示用户信息和退出登陆

#### 新增获取用户信息接口

`src/services/user/index.ts`

```ts
import { get, post } from '@/utils/request';
import { User } from '@/types';

const login = (params: User) => {
  return post('/front/user/login', params);
};

const getInfo = () => {
  return get('/front/user/getInfo', {});
};

export { login, getInfo };
```

`src/store/user/index.ts`

```ts
import { UserState } from '@/types';
import { get } from '@/utils/session';
import { getInfo } from '@/services/user';
import { UserInfo } from '@/types';

const RootState = get('RootState');
const userLoginInfo = get('userLoginInfo', localStorage);

// 如果缓存中存在用户模块数据，则将缓存中的数据设置给state
const state: UserState = (RootState && RootState.user) ||
  (userLoginInfo && { userLoginInfo, userInfo: null }) || {
    userLoginInfo: null, // 用户登陆信息
    userInfo: null, // 用户信息
  };

const getters = {
  /**
   * 获取用户登陆信息
   * @param state
   */
  userLoginInfo(state: UserState) {
    return state.userLoginInfo;
  },
  /**
   * 获取用户信息
   * @param state
   */
  userInfo(state: UserState): UserInfo {
    return state.userInfo;
  },
};

const mutations = {
  /**
   * 设置用户登陆信息
   * @param state
   * @param data
   */
  setUserLoginInfo(state: UserState, data: object) {
    state.userLoginInfo = data;
  },
  /**
   * 设置用户信息
   * @param state
   * @param data
   */
  setUserInfo(state: UserState, data: UserInfo) {
    state.userInfo = data;
  },
};

const actions = {
  /**
   * 调用接口获取用户信息
   * @param commit
   */
  async getInfo({ commit }: { commit: Function }) {
    try {
      const data = await getInfo();
      commit('setUserInfo', data);
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  },
};

export default { namespaced: true, state, getters, mutations, actions };
```

#### 页面

`src/layout/components/app-header.vue`

```vue
<template>
  <div class="header">
    <el-breadcrumb separator-class="el-icon-arrow-right">
      <el-breadcrumb-item :to="{ path: '/' }">首页</el-breadcrumb-item>
      <el-breadcrumb-item>活动管理</el-breadcrumb-item>
      <el-breadcrumb-item>活动列表</el-breadcrumb-item>
      <el-breadcrumb-item>活动详情</el-breadcrumb-item>
    </el-breadcrumb>
    <el-dropdown>
      <span class="el-dropdown-link">
        <el-avatar
          shape="square"
          :size="40"
          :src="
            userInfo.portrait ||
            'https://cube.elemecdn.com/9/c2/f0ee8a3c7c9638a54940382568c9dpng.png'
          "
        ></el-avatar>
        <i class="el-icon-arrow-down el-icon--right"></i>
      </span>
      <el-dropdown-menu slot="dropdown">
        <el-dropdown-item v-if="userInfo">
          {{ userInfo.userName }}
        </el-dropdown-item>
        <el-dropdown-item @click.native="onExit" divided>退出</el-dropdown-item>
      </el-dropdown-menu>
    </el-dropdown>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { mapActions, mapGetters, mapMutations } from 'vuex';

export default Vue.extend({
  name: 'AppHeader',
  computed: {
    ...mapGetters('user', ['userInfo']),
  },
  created() {
    this.getInfo();
  },
  methods: {
    ...mapMutations('user', ['setUserLoginInfo']),
    ...mapActions('user', ['getInfo']),
    onExit() {
      this.setUserLoginInfo(null);
      this.$router.push('/login');
    },
  },
});
</script>

<style lang="scss" scoped>
.header {
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;

  .el-dropdown-link {
    display: flex;
    align-items: center;
  }
}
</style>
<style>
.el-avatar > img {
  width: 100%;
}
</style>
```
