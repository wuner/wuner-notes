# 菜单管理

## 功能

- 查询菜单列表
- 添加/编辑/删除菜单

## 添加菜单

### 添加数据类型

`src/types/index.d.ts`

```ts
// ...
export interface Menu {
  id?: number; // 菜单ID，更新时须带上传给后台
  parentId: number; // 菜单父id
  name: string; // 菜单名称
  href: string; // 菜单路径
  icon?: string; // 菜单图标
  orderNum?: number; // 菜单序号
  description?: string; // 菜单描述
  shown?: boolean; // 是否显示
}
```

### 菜单接口封装

`src/services/menu/index.ts`

```ts
import request, { get } from '@/utils/request';
import { Menu } from '@/types';

/**
 * 保存或新增菜单
 * @param data
 */
const saveOrUpdate = (data: Menu) => {
  return request({
    url: '/boss/menu/saveOrUpdate',
    method: 'POST',
    data,
  });
};

/**
 * 获取编辑菜单页面信息
 * @param id 菜单ID
 */
const getEditMenuInfo = (id: string) => {
  return get('/boss/menu/getEditMenuInfo', { id });
};

export { saveOrUpdate, getEditMenuInfo };
```

### 页面

`src/views/menu/add.vue`

```vue
<template>
  <div class="add">
    <el-page-header
      style="margin-bottom: 20px"
      @back="goBack"
      content="添加菜单"
    ></el-page-header>
    <el-form ref="form" :rules="rules" :model="form" label-width="80px">
      <el-form-item label="菜单名称" prop="name">
        <el-input v-model="form.name"></el-input>
      </el-form-item>
      <el-form-item label="菜单路径" prop="href">
        <el-input v-model="form.href"></el-input>
      </el-form-item>
      <el-form-item label="上级菜单" prop="parentId">
        <el-select v-model="form.parentId" placeholder="请选择上级菜单">
          <el-option label="无上级菜单" :value="-1"></el-option>
          <el-option
            v-for="item in parentMenuList"
            :key="item.id"
            :label="item.name"
            :value="item.id"
          ></el-option>
        </el-select>
      </el-form-item>
      <el-form-item label="描述" prop="description">
        <el-input v-model="form.description"></el-input>
      </el-form-item>
      <el-form-item label="前端图标" prop="icon">
        <el-input v-model="form.icon"></el-input>
      </el-form-item>
      <el-form-item label="是否显示" prop="shown">
        <el-switch v-model="form.shown"></el-switch>
      </el-form-item>
      <el-form-item label="排序" prop="orderNum">
        <el-input v-model="form.orderNum"></el-input>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="onSubmit">提交</el-button>
        <el-button @click="resetForm">重置</el-button>
      </el-form-item>
    </el-form>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { Form } from 'element-ui';
import { getEditMenuInfo, saveOrUpdate } from '@/services/menu';

export default Vue.extend({
  name: 'menuAdd',
  data() {
    return {
      parentMenuList: [],
      form: {
        name: '',
        href: '',
        parentId: -1,
        description: '',
        icon: '',
        shown: false,
        orderNum: 0,
      },
      rules: {
        name: [
          {
            required: true,
            message: '请输入菜单名称',
            trigger: 'blur',
          },
        ],
        href: [
          {
            required: true,
            message: '请输入菜单路径',
            trigger: 'blur',
          },
        ],
      },
    };
  },
  methods: {
    goBack() {
      this.$router.go(-1);
    },
    onSubmit() {
      saveOrUpdate(this.form)
        .then(() => {
          this.$message('添加成功');
          this.goBack();
        })
        .catch((e) => this.$message.error(e));
    },
    resetForm() {
      console.log(this.$refs.form as Form);
      (this.$refs.form as Form).resetFields();
    },
  },
  async created() {
    try {
      const { parentMenuList } = await getEditMenuInfo('-1');
      this.parentMenuList = parentMenuList;
    } catch (e) {
      this.$message(e);
    }
  },
});
</script>
```

## 菜单列表

### 显示菜单列表

#### 新增接口

`src/services/menu/index.ts`

```ts
// ...

/**
 * 获取所有菜单
 */
const getAll = () => {
  return get('/boss/menu/getAll');
};
```

#### 页面

`src/views/menu/index.vue`

```vue
<template>
  <div class="menu">
    <el-button
      style="margin-bottom: 20px"
      type="primary"
      icon="el-icon-plus"
      @click="toAdd"
    >
      添加菜单
    </el-button>
    <el-table :data="menus" style="width: 100%">
      <el-table-column label="编号" type="index" />
      <el-table-column label="菜单名称" prop="name" min-width="180" />
      <el-table-column label="菜单级数" prop="level" min-width="180" />
      <el-table-column label="前端图标" prop="icon" min-width="180" />
      <el-table-column label="排序" prop="orderNum" min-width="180" />
      <el-table-column label="操作" min-width="180">
        <template slot-scope="scope">
          <el-button size="mini" @click="handleEdit(scope.row)">编辑</el-button>
          <el-button size="mini" type="danger" @click="handleDelete(scope.row)"
            >删除</el-button
          >
        </template>
      </el-table-column>
    </el-table>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { getAll } from '@/services/menu';
import { Menu } from '@/types';

export default Vue.extend({
  name: 'MenuIndex',
  data() {
    return {
      menus: [],
    };
  },
  methods: {
    toAdd() {
      this.$router.push('menu/add');
    },
    handleEdit(item: Menu) {
      console.log(item);
    },
    handleDelete(item: Menu) {
      console.log(item);
    },
    async loadMenus() {
      try {
        this.menus = await getAll();
      } catch (e) {
        this.$message.error(e);
      }
    },
  },
  created() {
    this.loadMenus();
  },
});
</script>
```

### 删除菜单

#### 新增接口

`src/services/menu/index.ts`

```ts
// ...

/**
 * 删除菜单
 */
const delMenu = (id: string | number) => {
  return request({
    url: `/boss/menu/${id}`,
    method: 'DELETE',
  });
};
```

#### 页面

`src/views/menu/index.vue`

```
handleDelete(item: Menu) {
  this.$confirm("是否删除该菜单？").then(async () => {
    try {
      await delMenu(item.id || "");
      this.loadMenus();
      this.$message.success("删除成功");
    } catch (e) {
      this.$message.error(e);
    }
  });
},
```

### 编辑菜单

由于添加与编辑几乎一样，所以我们将其抽取到一个组件

#### 组件

`src/views/menu/components/CreateOrEdit.vue`

```vue
<template>
  <div class="CreateOrEdit">
    <el-form ref="form" :rules="rules" :model="form" label-width="80px">
      <el-form-item label="菜单名称" prop="name">
        <el-input v-model="form.name"></el-input>
      </el-form-item>
      <el-form-item label="菜单路径" prop="href">
        <el-input v-model="form.href"></el-input>
      </el-form-item>
      <el-form-item label="上级菜单" prop="parentId">
        <el-select v-model="form.parentId" placeholder="请选择上级菜单">
          <el-option label="无上级菜单" :value="-1"></el-option>
          <el-option
            v-for="item in parentMenuList"
            :key="item.id"
            :label="item.name"
            :value="item.id"
          ></el-option>
        </el-select>
      </el-form-item>
      <el-form-item label="描述" prop="description">
        <el-input v-model="form.description"></el-input>
      </el-form-item>
      <el-form-item label="前端图标" prop="icon">
        <el-input v-model="form.icon"></el-input>
      </el-form-item>
      <el-form-item label="是否显示" prop="shown">
        <el-switch v-model="form.shown"></el-switch>
      </el-form-item>
      <el-form-item label="排序" prop="orderNum">
        <el-input v-model="form.orderNum"></el-input>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="onSubmit">提交</el-button>
        <el-button v-if="!edit" @click="resetForm">重置</el-button>
      </el-form-item>
    </el-form>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { getEditMenuInfo, saveOrUpdate } from '@/services/menu';
import { Form } from 'element-ui';

export default Vue.extend({
  name: 'CreateOrEdit',
  props: {
    edit: Boolean,
  },
  data() {
    return {
      parentMenuList: [],
      form: {
        name: '',
        href: '',
        parentId: -1,
        description: '',
        icon: '',
        shown: false,
        orderNum: 0,
      },
      rules: {
        name: [
          {
            required: true,
            message: '请输入菜单名称',
            trigger: 'blur',
          },
        ],
        href: [
          {
            required: true,
            message: '请输入菜单路径',
            trigger: 'blur',
          },
        ],
      },
    };
  },
  methods: {
    onSubmit() {
      saveOrUpdate(this.form)
        .then(() => {
          this.$message.success(this.edit ? '修改成功' : '添加成功');
          this.$router.go(-1);
        })
        .catch((e) => this.$message.error(e));
    },
    resetForm() {
      console.log(this.$refs.form as Form);
      (this.$refs.form as Form).resetFields();
    },
  },
  async created() {
    try {
      const { id } = this.$route.params;
      const { parentMenuList, menuInfo } = await getEditMenuInfo(id || '-1');
      this.parentMenuList = parentMenuList;
      this.form = menuInfo;
    } catch (e) {
      this.$message(e);
    }
  },
});
</script>
```

#### 页面

`src/views/menu/add.vue`

```vue
<template>
  <div class="add">
    <el-page-header
      style="margin-bottom: 20px"
      @back="goBack"
      content="添加菜单"
    ></el-page-header>
    <create-or-edit></create-or-edit>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrEdit from '@/views/menu/components/CreateOrEdit.vue';

export default Vue.extend({
  components: { CreateOrEdit },
  name: 'menuAdd',
  data() {
    return {};
  },
  methods: {
    goBack() {
      this.$router.go(-1);
    },
  },
});
</script>
```

`src/views/menu/edit.vue`

```vue
<template>
  <div class="add">
    <el-page-header
      style="margin-bottom: 20px"
      @back="goBack"
      content="编辑菜单"
    ></el-page-header>
    <create-or-edit edit />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrEdit from '@/views/menu/components/CreateOrEdit.vue';

export default Vue.extend({
  components: { CreateOrEdit },
  name: 'menuEdit',
  data() {
    return {};
  },
  methods: {
    goBack() {
      this.$router.go(-1);
    },
  },
});
</script>
```

`src/views/menu/index.vue`

```
handleEdit(item: Menu) {
  this.$router.push({
    name: "menuEdit",
    params: {
      id: item.id + "" || ""
    }
  });
},
```
