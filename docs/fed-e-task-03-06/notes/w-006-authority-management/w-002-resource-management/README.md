# 资源管理

## 功能

- 按条件分页查询资源
- 添加/编辑/删除资源
- 资源分类添加、编辑、展示

## 资源分类添加、编辑、展示

### 新增数据类型

`src/types/index.d.ts`

```ts
export interface ResourceCategory {
  id?: number | string;
  name?: string;
  sort?: number | string;
}
```

### 新增接口 API

`src/services/resource/index.ts`

```ts
/**
 * 查询资源分类列表
 */
export const getCategoryAll = () => {
  return get('/boss/resource/category/getAll');
};

/**
 * 保存或更新资源分类
 */
export const saveOrUpdateCategory = (data: ResourceCategory) => {
  return request({
    url: `/boss/resource/category/saveOrderUpdate`,
    method: 'POST',
    data,
  });
};

/**
 * 删除资源分类，如果资源分类下有资源，不允许删除
 */
export const delCategory = (id: string | number) => {
  return request({
    url: `/boss/resource/category/${id}`,
    method: 'DELETE',
  });
};
```

### 页面

`src/views/resource/category.vue`

```vue
<template>
  <div class="resource-category">
    <el-page-header
      style="margin-bottom: 20px"
      @back="$router.go(-1)"
      content="资源分类"
    ></el-page-header>
    <el-button
      style="margin-bottom: 20px"
      type="primary"
      icon="el-icon-plus"
      @click="show = true"
    >
      添加
    </el-button>
    <el-table v-loading="loading" :data="categoryList" style="width: 100%">
      <el-table-column label="编号" type="index" />
      <el-table-column label="名称" prop="name" min-width="180" />
      <el-table-column label="排序" prop="sort" min-width="180" />
      <el-table-column label="创建时间" prop="createdTime" min-width="180">
        <template slot-scope="scope">
          {{ scope.row.createdTime | date }}
        </template>
      </el-table-column>
      <el-table-column label="操作" min-width="180">
        <template slot-scope="scope">
          <el-button size="mini" @click="handleEdit(scope.row)">编辑</el-button>
          <el-button size="mini" type="danger" @click="handleDelete(scope.row)"
            >删除</el-button
          >
        </template>
      </el-table-column>
    </el-table>
    <el-dialog title="添加分类" :visible.sync="show">
      <el-form ref="form" :rules="rules" :model="form">
        <el-form-item label="名称" prop="name">
          <el-input v-model="form.name" autocomplete="off"></el-input>
        </el-form-item>
        <el-form-item label="排序" prop="sort">
          <el-input v-model.number="form.sort" autocomplete="off"></el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="onCancel">取 消</el-button>
        <el-button type="primary" @click="onSubmit">确 定</el-button>
      </div>
    </el-dialog>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import {
  delCategory,
  getCategoryAll,
  saveOrUpdateCategory,
} from '@/services/resource';
import { ResourceCategory } from '@/types';
import { Form } from 'element-ui';

export default Vue.extend({
  name: 'resourceCategory',
  data() {
    return {
      loading: false,
      form: {
        name: '',
        sort: '',
      } as ResourceCategory,
      rules: {
        name: {
          required: true,
          message: '请输入名称',
          trigger: 'blur',
        },
        sort: [
          {
            required: true,
            message: '请输入排序',
            trigger: 'blur',
          },
          {
            type: 'number',
            message: '排序必须为数字值',
          },
        ],
      },
      show: false,
      categoryList: [],
    };
  },
  methods: {
    handleEdit(item: ResourceCategory) {
      this.show = true;
      this.form = item;
    },
    handleDelete(item: ResourceCategory) {
      this.$confirm('是否删除该资源分类？').then(async () => {
        try {
          await delCategory(item.id || '');
          this.loadCategoryAll();
          this.$message.success('删除成功');
        } catch (e) {
          this.$message.error(e);
        }
      });
    },
    async loadCategoryAll() {
      try {
        this.loading = true;
        this.categoryList = await getCategoryAll();
        this.loading = false;
      } catch (e) {
        this.$message.error(e);
      }
    },
    onCancel() {
      this.show = false;
      (this.$refs.form as Form).resetFields();
    },
    onSubmit(edit: boolean) {
      (this.$refs.form as Form).validate((valid: boolean) => {
        if (valid) {
          saveOrUpdateCategory(this.form)
            .then(() => {
              this.$message.success(edit ? '修改成功' : '添加成功');
              this.loadCategoryAll();
              this.onCancel();
            })
            .catch((e) => this.$message.error(e));
        } else {
          return false;
        }
      });
    },
  },
  created() {
    this.loadCategoryAll();
  },
});
</script>
```

## 资源添加

### 新增数据类型

`src/types/index.d.ts`

```ts
export interface Resource {
  id?: number | string; // 资源ID，新建时为空
  name: string; // 资源名称
  categoryId: number | string; // 资源分类ID
  url: string; // 资源路径
  description?: string; // 资源描述
}
```

### 新增接口 API

`src/services/resource/index.ts`

```ts
import request from '@/utils/request';
import { Resource } from '@/types';

/**
 * 保存或者更新资源
 * @param data
 */
export const saveOrUpdate = (data: Resource) => {
  return request({
    url: '/boss/resource/saveOrUpdate',
    method: 'POST',
    data,
  });
};

/**
 * 获取资源
 */
export const getResource = (id: string) => {
  return get(`/boss/resource/${id}`);
};
```

### 页面

`src/views/resource/components/CreateOrEdit.vue`

```vue
<template>
  <div class="CreateOrEdit">
    <el-form ref="form" :rules="rules" :model="form" label-width="80px">
      <el-form-item label="资源名称" prop="name">
        <el-input v-model="form.name"></el-input>
      </el-form-item>
      <el-form-item label="资源路径" prop="url">
        <el-input v-model="form.url"></el-input>
      </el-form-item>
      <el-form-item label="资源分类" prop="categoryId">
        <el-select v-model="form.categoryId" placeholder="请选择资源分类">
          <el-option
            v-for="item in categoryList"
            :key="item.id"
            :label="item.name"
            :value="item.id"
          ></el-option>
        </el-select>
      </el-form-item>
      <el-form-item label="描述" prop="description">
        <el-input v-model="form.description"></el-input>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="onSubmit">提交</el-button>
        <el-button v-if="!edit" @click="resetForm">重置</el-button>
      </el-form-item>
    </el-form>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { getCategoryAll, saveOrUpdate, getResource } from '@/services/resource';
import { Form } from 'element-ui';

export default Vue.extend({
  name: 'CreateOrEdit',
  props: {
    edit: Boolean,
  },
  data() {
    return {
      categoryList: [],
      form: {
        name: '',
        url: '',
        categoryId: '',
        description: '',
      },
      rules: {
        name: [
          {
            required: true,
            message: '请输入资源名称',
            trigger: 'blur',
          },
        ],
        categoryId: [
          {
            required: true,
            message: '请选择资源分类',
            trigger: 'blur',
          },
        ],
        url: [
          {
            required: true,
            message: '请输入资源路径',
            trigger: 'blur',
          },
        ],
      },
    };
  },
  methods: {
    onSubmit() {
      (this.$refs.form as Form).validate((valid: boolean) => {
        if (valid) {
          saveOrUpdate(this.form)
            .then(() => {
              this.$message.success(this.edit ? '修改成功' : '添加成功');
              this.$router.go(-1);
            })
            .catch((e) => this.$message.error(e));
        } else {
          return false;
        }
      });
    },
    resetForm() {
      console.log(this.$refs.form as Form);
      (this.$refs.form as Form).resetFields();
    },
  },
  async created() {
    this.categoryList = await getCategoryAll();
    if (this.edit) {
      this.form = await getResource(this.$route.params.id);
    }
  },
});
</script>
```

`src/views/resource/add.vue`

```vue
<template>
  <div class="add">
    <el-page-header
      style="margin-bottom: 20px"
      @back="goBack"
      content="添加资源"
    ></el-page-header>
    <create-or-edit></create-or-edit>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrEdit from '@/views/resource/components/CreateOrEdit.vue';

export default Vue.extend({
  components: { CreateOrEdit },
  name: 'resourceAdd',
  data() {
    return {};
  },
  methods: {
    goBack() {
      this.$router.go(-1);
    },
  },
});
</script>
```

## 资源列表展示、删除、编辑

### 新增数据类型

`src/types/index.d.ts`

```ts
export interface ResourceFilter {
  id?: number;
  name?: string;
  startCreateTime?: string;
  url?: string;
  categoryId?: number | string;
  endCreateTime?: string;
  current?: number;
  size?: number;
}
```

### 新增接口 API

`src/services/resource/index.ts`

```ts
/**
 * 删除资源
 */
export const delResource = (id: string | number) => {
  return request({
    url: `/boss/resource/${id}`,
    method: 'DELETE',
  });
};

/**
 * 按条件分页查询资源
 * @param data
 */
export const getResourcePages = (data: ResourceFilter) => {
  return request({
    url: '/boss/resource/getResourcePages',
    method: 'POST',
    data,
  });
};
```

### 页面

`src/views/resource/index.vue`

```vue
<template>
  <div class="resource">
    <el-form ref="form" :model="resourceFilter" class="demo-form-inline">
      <el-form-item label="资源名称" prop="name">
        <el-input
          v-model="resourceFilter.name"
          placeholder="资源名称"
        ></el-input>
      </el-form-item>
      <el-form-item label="资源路径" prop="url">
        <el-input
          v-model="resourceFilter.url"
          placeholder="资源路径"
        ></el-input>
      </el-form-item>
      <el-form-item label="资源分类" prop="categoryId">
        <el-select
          v-model="resourceFilter.categoryId"
          placeholder="全部"
          clearable
        >
          <el-option
            v-for="category in categoryList"
            :key="category.id"
            :label="category.name"
            :value="category.id"
          ></el-option>
        </el-select>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="onSubmit">查询</el-button>
        <el-button @click="resetForm">重置</el-button>
      </el-form-item>
    </el-form>
    <el-button
      style="margin-bottom: 20px"
      type="primary"
      icon="el-icon-plus"
      @click="toAdd"
    >
      添加资源
    </el-button>
    <el-button style="margin-bottom: 20px" type="primary" @click="toCategory">
      资源分类
    </el-button>
    <el-table v-loading="loading" :data="resources" style="width: 100%">
      <el-table-column label="编号" type="index" />
      <el-table-column label="资源名称" prop="name" min-width="180" />
      <el-table-column label="资源路径" prop="url" min-width="180" />
      <el-table-column label="描述" prop="description" min-width="180" />
      <el-table-column label="添加时间" prop="createdTime" min-width="180">
        <template slot-scope="scope">
          {{ scope.row.createdTime | date }}
        </template>
      </el-table-column>
      <el-table-column label="操作" min-width="180">
        <template slot-scope="scope">
          <el-button size="mini" @click="handleEdit(scope.row)">编辑</el-button>
          <el-button size="mini" type="danger" @click="handleDelete(scope.row)"
            >删除</el-button
          >
        </template>
      </el-table-column>
    </el-table>
    <el-pagination
      v-loading="loading"
      style="margin-top: 20px"
      @size-change="handleSizeChange"
      @current-change="handleCurrentChange"
      :current-page="resourceFilter.current"
      :page-sizes="[10, 20, 30, 40]"
      :page-size="resourceFilter.size"
      layout="total, sizes, prev, pager, next, jumper"
      :total="total"
    >
    </el-pagination>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { Form } from 'element-ui';
import {
  getResourcePages,
  getCategoryAll,
  delResource,
} from '@/services/resource';
import { Resource } from '@/types';

export default Vue.extend({
  name: 'ResourceIndex',
  data() {
    return {
      loading: false,
      categoryList: [],
      resourceFilter: {
        current: 1,
        size: 10,
        name: '',
        url: '',
        categoryId: '',
      },
      total: 0,
      resources: [],
    };
  },
  methods: {
    toAdd() {
      this.$router.push('resource/add');
    },
    toCategory() {
      this.$router.push('resource/category');
    },
    async loadResourcePages() {
      try {
        this.loading = true;
        const { records, total } = await getResourcePages(this.resourceFilter);
        this.loading = false;
        this.resources = records;
        this.total = total;
      } catch (e) {
        this.$message.error(e);
      }
    },
    onSubmit() {
      this.resourceFilter.current = 1;
      this.loadResourcePages();
    },
    resetForm() {
      (this.$refs.form as Form).resetFields();
    },
    handleEdit(item: Resource) {
      this.$router.push({
        name: 'resourceEdit',
        params: {
          id: item.id + '' || '',
        },
      });
    },
    handleDelete(item: Resource) {
      this.$confirm('是否删除该资源？').then(async () => {
        try {
          await delResource(item.id || '');
          this.loadResourcePages();
          this.$message.success('删除成功');
        } catch (e) {
          this.$message.error(e);
        }
      });
    },
    handleSizeChange(size: number) {
      this.resourceFilter.current = 1;
      this.resourceFilter.size = size;
      this.loadResourcePages();
    },
    handleCurrentChange(current: number) {
      this.resourceFilter.current = current;
      this.loadResourcePages();
    },
  },
  async created() {
    this.categoryList = await getCategoryAll();
    this.loadResourcePages();
  },
});
</script>
```

`src/views/resource/edit.vue`

```vue
<template>
  <div class="add">
    <el-page-header
      style="margin-bottom: 20px"
      @back="goBack"
      content="编辑资源"
    ></el-page-header>
    <create-or-edit edit />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrEdit from '@/views/resource/components/CreateOrEdit.vue';

export default Vue.extend({
  components: { CreateOrEdit },
  name: 'resourceEdit',
  data() {
    return {};
  },
  methods: {
    goBack() {
      this.$router.go(-1);
    },
  },
});
</script>
```
