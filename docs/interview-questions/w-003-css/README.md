# [css 基础面试题](https://juejin.cn/post/6905539198107942919)

## CSS 选择符有哪些

1. > id 选择器( # myid)
2. > 类选择器(.myclassname)
3. > 标签选择器(div, h1, p)
4. > 相邻选择器(h1 + p)
5. > 子选择器(ul< li)
6. > 后代选择器(li a)
7. > 通配符选择器( \* )
8. > 属性选择器(a[rel = "external"])
9. > 伪类选择器(a: hover, li: nth - child)

## 哪些属性可以继承

- > 可继承的样式： font-size font-family color, UL LI DL DD DT;
- > 不可继承的样式：border padding margin width height ;

## 优先级算法如何计算

- > 优先级就近原则，同权重情况下样式定义最近者为准;
- > 载入样式以最后载入的定位为准;
- > 优先级为: important > 内联 > id > class > tag

## CSS3 新增伪类

1. > p:first-of-type 选择属于其父元素的首个元素的每个元素。
2. > p:last-of-type 选择属于其父元素的最后元素的每个元素。
3. > p:only-of-type 选择属于其父元素唯一的元素的每个元素。
4. > p:only-child 选择属于其父元素的唯一子元素的每个元素。
5. > p:nth-child(2) 选择属于其父元素的第二个子元素的每个元素。
6. > :enabled :disabled 控制表单控件的禁用态。
7. > :checked 单选框或复选框被选中。

## 如何居中 div？如何居中一个浮动元素？

### DIV 水平居中

设置 DIV 的宽高，使用 margin 设置边距 0 auto，CSS 自动算出左右边距，使得 DIV 居中。

```css
div {
  width: 100px;
  height: 100px;
  margin: 0 auto;
}
```

### 实现 DIV 水平、垂直居中

要让 DIV 水平和垂直居中，必需知道该 DIV 得宽度和高度，然后设置位置为绝对位置，距离页面窗口左边框和上边框的距离设置为 50%，这个 50%就是指页面窗口的宽度和高度的 50%，最后将该 DIV 分别左移和上移，左移和上移的大小就是该 DIV 宽度和高度的一半。

```css
div {
  width: 100px;
  height: 100px;
  position: absolute;
  left: 50%;
  top: 50%;
  margin: -50px 0 0 -50px;
}
```

## [从浏览器渲染层面解析 css3 动效优化原理](https://my.oschina.net/o2team/blog/5268715)

## [CSS 奇思妙想：超级酷炫的边框动画](https://my.oschina.net/sl1673495/blog/4917295)
